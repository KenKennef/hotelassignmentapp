/**
 * Created by linas on 21 11 2015.
 */
$(document).ready(function () {
    $('.meal_add').click(function () {
        var clicked = $(this);
        addFood(clicked);
    });
    $('.meal_subtract').click(function () {
        var clicked = $(this);
        subFood(clicked);
    });

    $("#meal_cart_button").click(function(){
        calculateTotal();
    });

    $("#meal_cancel_button").click(function(){
        $(".meal_quantity").each(function(){
            $(this).val("0");
        });
    });

    function addFood(element) {
        var target = element.parent().parent().find(".quantity_cell").find(".meal_quantity");
        var title = element.parent().parent().parent().parent().parent().parent().find(".catering_list_item_center").find(".catering_list_title").text();
        var price = element.parent().parent().parent().find("tr").find("td").find(".catering_price").text();
        var change = parseInt(target.val()) + 1;
        target.val(change);
        if (parseInt(change) > 0) {
            target.css('color', 'white');
            addToCart(title, price);
        } else {
            target.css('color', '#474747 !important');
        }
    }

    function subFood(element) {
        var target = element.parent().parent().find(".quantity_cell").find(".meal_quantity");
        var change = parseInt(target.val()) - 1;
        if (parseInt(target.val()) >= 1) {
            target.val(change);
        }
    }

    function addToCart(name, price){
        $("#meal_order_cart").append("<li><p class=\"meal_cart_item_name\">"+name+"</p><p class=\"meal_cart_item_price\">"+price+"</p></li>");
    }

    function calculateTotal(){
        var total = 0;

        $(".meal_cart_item_price").each(function(){
            var inner = $(this).text();
            var nodollar = inner.replace("$", "");
            var final = nodollar.replace(".","");
            var price = parseInt(final);
            total = total+price;
        });
        var divided = total/100;

        $("#meal_order_total").text("");
        $("#meal_order_total").append("$"+divided);
    }

});

//DB POPULATING
//
//document.addEventListener("deviceready", onDeviceReady, false);
//
//function onDeviceReady() {
//    var db = sqlitePlugin.openDatabase({name: "common.db", location:2, createFromLocation: 1});
//
//    db.transaction(function (tx) {
//        tx.executeSql('SELECT * FROM "Food";', [], function (tx, res) {
//            console.log("res.rows.length: " + res.rows.length + " -- should be 1");
//            console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
//
//        });
//    }, function (e) {
//        console.log("ERROR: " + e.message);
//        $("#main_title").html("ERROR IN DB");
//    });
//}/**
